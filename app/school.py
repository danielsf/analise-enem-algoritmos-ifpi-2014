# -*- coding: utf-8 -*-

class School(object):

	#Properties:
	ranking = 0
	nome = ""
	municipio = ""
	uf = ""
	rede = ""

	indicador_permanencia = ""
	indicador_socioeconomico = ""
	
	avg_objetivas = 0.0
	avg_linguagens = 0.0
	avg_matematica = 0.0
	avg_natureza = 0.0
	avg_humanas = 0.0
	avg_redacao = 0.0

	def __init__(self, lista):
		#Melhorar: TA HORRIVEL :( -> Jogar pro INIT e Validar:
		self.ranking = int(lista[0])
		self.nome = lista[1]
		self.municipio = lista[2]
		self.uf = lista[3]
		self.rede = lista[4]

		self.ind_permanencia = lista[5]
		self.ind_socioeconomico = lista[6]
		
		self.avg_objetivas = self.normalize_avg(lista[7]) 
		self.avg_linguagens = self.normalize_avg(lista[8])
		self.avg_matematica = self.normalize_avg(lista[9])
		self.avg_natureza = self.normalize_avg(lista[10])
		self.avg_humanas = self.normalize_avg(lista[11])
		self.avg_redacao = self.normalize_avg(lista[12])
		#
	
	def __repr__(self):
		return "[Rank: %s, Nome: %s, UF: %s, Medias: %s]" % (self.ranking, self.nome, self.uf, self.get_general_avg())

	def normalize_avg(self, item):
		return float(item.replace(",","."))
	
	def get_higher_avg(self):
		#
		return max(self.avg_objetivas, self.avg_linguagens, self.avg_matematica, 
		self.avg_natureza, self.avg_humanas, self.avg_redacao)

	def get_general_avg(self):
		#DICIONARIO
		media = {
			"Provas Objetivas" : self.avg_objetivas, 
			"Linguagens" : self.avg_linguagens,
			"Matematica" : self.avg_matematica,
			"Natureza" : self.avg_natureza,
			"Humanas" : self.avg_humanas,
			"Redacao" : self.avg_redacao
		}

		return media

	def get_type_for_avg(self):
		major = self.get_higher_avg()
		dicionario = self.get_general_avg()
		#
		for i in dicionario:
			if(major == dicionario[i]):
				return i

	
		#Brekar aqui -> mais coisas;