# -*- coding: utf-8 -*-

def read_data(filename = '../data/enem2014_escolas.csv'):
	lista = []

	file = open(filename)

	for f in file:
		item = f.strip().split(";")
		lista.append(item[2: 15])

	file.close()

	return lista

if __name__ == '__main__':
	print(read_data()[0])