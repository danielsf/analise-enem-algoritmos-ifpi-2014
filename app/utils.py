# -*- coding: utf-8 -*-

redes = {'F':'FEDERAL', 'M':'MUNICIPAL', 'P':'PRIVADA', 'E':'ESTADUAL'}

capitais = {'AC':'RIO BRANCO',
'AP': 'MACAPA', 
'AM': 'MANAUS', 
'PA': 'BELEM',
'RO' : 'PORTO VELHO',
'RR' : 'BOA VISTA',
'AL' : 'MACEIO',
'BA' : 'SALVADOR',
'CE' : 'FORTALEZA',
'MA' : 'SAO LUIS',
'PE' : 'RECIFE',
'PB' : 'JOAO PESSOA',
'RN' : 'NATAL',
'PI' : 'TERESINA',
'SE' : 'ARACAJU',
'SC' : 'FLORIANOPOLIS',
'PR' : 'CURITIBA',
'RS' : 'PORTO ALEGRE',
'RJ' : 'RIO DE JANEIRO',
'SP' : 'SAO PAULO',
'ES' : 'VITORIA',
'MG' : 'BELO HORIZONTE',
'MT' : 'CAMPO GRANDE',
'MS' : 'CUIABA',
'TO' : 'PALMAS',
'GO' : 'GOIANIA',
'DF' : 'BRASILIA'}

regioes = {'NO':('AC', 'AP', 'AM','PA','RO', 'RR'), 
'NE':('AL', 'BA', 'CE', 'MA', 'PE', 'PB', 'RN', 'PI', 'SE'), 
'SU':('SC', 'PR', 'RS'), 
'SE':('RJ', 'SP', 'ES', 'MG'), 
'CO':('MT', 'MS', 'TO', 'GO', 'DF')}
#

#Pesquisa por Nome:
def find_str(lista, str_find):
	temp = []

	for i in lista:
		if (str_find.lower() in i.nome.lower()): #Normalizando pesquisas;
			temp.append(i)

	#return temp # Ordena:
	return sort_by_rule(temp, rule = 'name', n = len(temp))

#Metodos de Ordenacao:
def  sort_name(lista):
	return lista.nome

def  sort_rank(lista):
	return lista.ranking

def  sort_city(lista):
	return lista.municipio

def  sort_uf(lista):
	return lista.uf

def  sort_rede(lista):
	return lista.rede

#TODOD => sort by media, group by 
def sort_by_lin(lista):
	return lista.avg_linguagens

def sort_by_mat(lista):
	return lista.avg_matematica

def sort_by_hum(lista):
	return lista.avg_humana

def sort_by_nat(lista):
	return lista.avg_natureza

def sort_by_red(lista):
	return lista.avg_redacao

def sort_by_obj(lista):
	return lista.avg_objetivas

def map_avg(): #NOT WORKS YET

	tipo = {
		"OBJ":"Provas Objetivas", 
		"LIN":"Linguagens",
		"MAT":"Matematica",
		"NAT":"Natureza",
		"HUM":"Humanas",
		"RED":"Redacao"
	}

	return tipo

def top_regioes(lista):
	temp = []

	for i in regioes:
		atual = sort_by_rule(lista, regiao = i, n = 1)
		temp += atual

	return sort_by_rule(temp, n = len(temp))

def get_avg_all(lista):
	media = 0
	qtd_escolas = len(lista)

	for i in lista:
		media += (i.avg_objetivas + i.avg_redacao) / 2

	return media / qtd_escolas

def higher_of_average(lista, value = 'avg'):
	if(avg):
		temp = []

		escolas = sort_by_rule(lista, n = len(lista))
		
		for i in escolas:
			#if(i.)
			temp.append(i)


#Ordenacao via metodo
def sort_by_rule(lista, n = 10, regiao = None, uf = None, municipio = None, rede = None, rule = 'rank', media_type = None):
	#mexer nesses temps
	
	if(regiao):
		temp = []

		for i in lista:
			if(i.uf in regioes[regiao]):
				temp.append(i)
		
		lista = temp

	if(uf):
		temp = []
		
		for i in lista:
			if(i.uf == uf):
				temp.append(i)
		
		lista = temp

	if(municipio):
		temp = []
		
		for i in lista:
			if(i.municipio == municipio):
				temp.append(i)
		
		lista = temp

	if(rede):
		temp = []

		for i in lista:
			if(i.rede.upper() == redes[rede]): #Normalizando a pesquisa
				temp.append(i)
		
		lista = temp				

	#Define A regra de Ordenacao:
	if(rule == 'name'):
		return sorted(lista, key=sort_name)[:n]
	elif(rule == 'rede'):
		return sorted(lista, key=sort_rede)[:n]
	elif(rule == 'city'):
		return sorted(lista, key=sort_city)[:n]
	elif(rule == 'uf'):
		return sorted(lista, key=sort_uf)[:n]
	elif(rule == 'media'):
		
		if(media_type):
			#print media_type
			if(media_type == 'LIN'):
				return sorted(lista, key=sort_by_lin, reverse=True)[:n]
			elif(media_type == 'MAT'):
				return sorted(lista, key=sort_by_mat, reverse=True)[:n]
			elif(media_type == 'NAT'):
				return sorted(lista, key=sort_by_nat, reverse=True)[:n]
			elif(media_type == 'HUM'):
				return sorted(lista, key=sort_by_hum, reverse=True)[:n]
			elif(media_type == 'RED'):
				return sorted(lista, key=sort_by_red, reverse=True)[:n]
			else:
				return sorted(lista, key=sort_by_obj, reverse=True)[:n]		
	else: #Caso seja Rank
		return sorted(lista, key=sort_rank)[:n]
		
#
def awards(lista, capital = False, n = 10, estados = True, rule = 'rank'):
	#
	temp = []

	for i in capitais:
		temp += sort_by_rule(lista, n = 1, uf = i)
	#
	return sort_by_rule(temp, n = len(temp))[:n]


def count_set(lista):
	return len(lista)

def is_capital(escola):
	return capitais[escola.uf] == escola.municipio
#
def show_set(lista, extra = None):

	if(not lista): #Se a lista estiver vazia
		print("\nSem resultados para a consulta\n")
	else:
		#
		if(extra):
			tipo = map_avg()

			print(" %s - %s - %s - %s - %s \n" % ("N", "Rank", "Nome", "Municipio", tipo[extra]))
			
			for i in range(len(lista)):
				print(" %s - %s - %s - %s - %s - %s" % ((i+1), lista[i].ranking, lista[i].nome, lista[i].uf, lista[i].municipio, lista[i].get_general_avg()[tipo[extra]]))
			print("")
		else:
			print(" %s - %s - %s - %s \n" % ("N", "Rank", "Nome", "Municipio"))
			
			for i in range(len(lista)):
				print(" %s - %s - %s - %s - %s" % ((i+1), lista[i].ranking, lista[i].nome, lista[i].uf, lista[i].municipio))
			print("")
		#
#