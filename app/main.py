# -*- coding : utf-8 -*-

from utils import *
from importer import *
from school import *
###CARREGANDO:

header = " ***********ENEM 2014***********\n"

str_face = '''
 01 - Pesquisar Escola por Nome
 02 - Mostrar o Top 10 nacional
 03 - Mostrar o Top N nacional
 04 - Mostrar o Top N estadual
 05 - Mostrar o Top N de um municipio
 06 - Mostrar o Top N de escolas por rede no brasil
 07 - Mostrar a contagem de escolas por estado
 08 - Mostrar o Top N entre as melhores de cada Estado
 09 - Mostrar o Top N entre as provas
 10 - Mostrar o Top entre regioes
\n 00 - Sair \n'''

footer = " *********************************\n"

str_face = header + str_face + footer

dados = read_data()
escolas = []

for i in dados:
	escola = School(i)
	escolas.append(escola)

while(True):
	
	op = int(input(str_face))

	if(op == 0):
		#Saindo:
		print("Bye")
		break
	else:
		if(op == 1):
			str_in = input("Digite o nome ou parte do nome: ")
			show_set(find_str(escolas, str_in))
			#1
		elif(op == 2):
			show_set(sort_by_rule(escolas))
			#2	
		elif(op == 3):
			num = int(input("Digite a quantidade de escolas: "))
			show_set(sort_by_rule(escolas, n = num))
			#3
		elif(op == 4):
			num = int(input("Digite a quantidade de escolas: "))
			estado = input("Digite o nome da UF: ")

			show_set(sort_by_rule(escolas, n = num, uf = estado))
			#4
		elif(op == 5):
			num = int(input("Digite a quantidade de escolas: "))
			estado = input("Digite o nome da UF: ")
			city = input("Digite um municipio da UF %s:" % estado)

			show_set(sort_by_rule(escolas, n = num, uf = estado, municipio = city))
			#5
		elif(op == 6):
			num = int(input("Digite a quantidade de escolas: "))
			setor = input("Digite a rede a pesquisar[P/M/E/F]: ")
			show_set(sort_by_rule(escolas, rede = setor, n = num))
			#6
		elif(op == 7):
			estado = input("Digite o nome da UF: ")
			#TODO JOGAR PRA VIEW:
			print("Estado %s, quantidade de alunos: %s" % (estado, count_set(sort_by_rule(escolas, uf = estado, n = len(escolas)))))
			
			for i in redes:
				print("Estado %s, Rede: %s, quantidade de alunos: %s" % (estado, redes[i], count_set(sort_by_rule(escolas, uf = estado, n = len(escolas), rede = i))))				
			#7
		elif(op == 8):
			num = int(input("Digite a quantidade de resultados: "))
			show_set(awards(escolas, n = num))	
			#8		
		elif(op == 9):
			disciplina = input("Selecione uma disciplina pra comparar (LIN/HUM/NAT/MAT/OBJ/RED): ")
			show_set(sort_by_rule(escolas, media_type = disciplina, rule = 'media'), extra = disciplina)
			#9
		elif(op == 10):
			show_set(top_regioes(escolas))
			#10
		else:
			print("Opcao Invalida, Tente Novamente...")
		